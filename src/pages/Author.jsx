import React, { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Table, Image } from 'react-bootstrap';
import { BrowserRouter as Router, useHistory } from "react-router-dom";
import { addAuthor, deleteAuthorById, fetchAllAuthor, fetchAuthorById, updateAuthorById, uploadImage } from '../services/AuthorService';
import { useLocation } from "react-router";
import query from 'query-string';

export default function Author() {

    const [myImage, setMyImage] = useState(null);
    const [browsedImage, setBrowsedImage] = useState("");

    const [author, setAuthor] = useState([]);
    const history = useHistory();
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");

    const placeholder = "https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png";

    const { search } = useLocation()
    console.log("Location:", search);

    let { id } = query.parse(search)
    console.log("id query:", id);

    //Browse image from and add to image tag
    function onBrowsedImage(e) {
        setMyImage(e.target.files[0]);
        setBrowsedImage(URL.createObjectURL(e.target.files[0]));
    }

    //Add/Update onAddAuthor to api
    async function onAddAuthor() {
        if (search === "") {
            const url = myImage && await uploadImage(myImage);
            const author = {
                name,
                email,
                image: url ? url : placeholder,
            };
            await addAuthor(author);
        } else {
            const url = myImage && await uploadImage(myImage);
            const author = {
                name,
                email,
                image: url ? url : browsedImage,
            };
            await updateAuthorById(id, author)
        }
        const result = await fetchAllAuthor();
        console.log("Author:", result);
        setAuthor(result);
        setName("")
        setEmail("")
        setBrowsedImage(placeholder)
        setMyImage("")
        history.push("")
    }

    //fetch all author
    useEffect(async () => {
        const result = await fetchAllAuthor();
        console.log("Author: ", result);
        setAuthor(result);
        if (search == "") {
            setName("");
            setEmail("");
            setBrowsedImage(placeholder);
        } else {
            const result = await fetchAuthorById(id);
            setName(result.name);
            setEmail(result.email);
            setBrowsedImage(result.image);
        }
    }, [id]);

    //Delete Author by ID
    async function onDeleteAuthorById(id) {
        await deleteAuthorById(id);
        const temp = author.filter(item => {
            return item._id !== id
        })
        setAuthor(temp)
    }

    return (
        <div>
            <Router>
                <Row className="my-4">
                    <Col md={8}>
                        <Form>
                            <h2>Author</h2>
                            <Form.Group className="mb-3" controlId="formBasicPassword">
                                <Form.Label>Author Name</Form.Label>
                                <Form.Control value={name} onChange={(e) => setName(e.target.value)} type="text" placeholder="Author Name" />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Email address</Form.Label>
                                <Form.Control
                                    value={email} onChange={(e) => setEmail(e.target.value)} type="email" placeholder="Enter email" />
                                <Form.Text className="text-muted">
                                    We'll never share your email with anyone else.
                                </Form.Text>
                            </Form.Group>
                            <Button onClick={onAddAuthor} variant="primary">{search ? "Update" : "Add"}</Button>
                        </Form>
                    </Col>
                    <Col md={4}>
                        <div style={{ width: "300px" }}>
                            <label htmlFor="myfile">
                                <Image width="100%" src={browsedImage ? browsedImage : placeholder} />
                            </label>
                        </div>
                        <input
                            onChange={onBrowsedImage}
                            id="myfile"
                            type="file"
                            style={{ display: "" }}
                        />
                    </Col>
                </Row>
                <Row>
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Author Name</th>
                                <th>Email</th>
                                <th>Image Profile</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                author.map((item, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{item._id.slice(0, 8)}</td>
                                            <td>{item.name}</td>
                                            <td>{item.email}</td>
                                            <td>
                                                <Image width="100" src={item.image}/>
                                            </td>
                                            <td>
                                                <Button onClick={() => history.push(`/?id=${item._id}`)} variant="warning">Edit</Button>{' '}
                                                <Button onClick={() => onDeleteAuthorById(item._id)} variant="danger">Delete</Button>
                                            </td>
                                        </tr>
                                    );
                                })
                            }

                        </tbody>
                    </Table>
                </Row>
            </Router>


        </div>
    )
}
