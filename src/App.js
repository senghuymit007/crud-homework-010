import "./App.css";
import MenuBar from "./components/MenuBar";
import Author from "./pages/Author";
import { Container } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function App() {
  return (
    <>
    <Router>
    <MenuBar />
      <Container>
        <Switch>
          <Route path="/" component={Author}/>
        </Switch>
      </Container>
    </Router>
     
    </>
  );
}

export default App;
